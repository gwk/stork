# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

'''
interpreter macro expand and top-level interpreter loop.
'''

import stork_eval # for print_trace
from stork_eval import *

trace_expand_char = '◇' # white diamond
trace_post_expand_char = '▫' # white small square


def transform_struct(s, f):
  # f is the polymorphic function to apply to fields; takes a single object argument.
  t = s._type_of
  assert t.kind._tag == TI_struct # this is a struct type.
  type_els = t.struct._vals # get the python list of Type-el.
  items = []
  for e in Type_els:
    n = e.name # field name
    items.append((n, f(s[n])))
  return mk_Obj_with_items(t, items)


def eval_Expansion(expansion, env):
  c = expansion.callee
  if c._type_of is not Sym:
    error('expansion callee must be a symbol; found: {}', c)
  m = env.resolve(c._val)
  if m._type_of is not Func and m.kind._tag != TI_func_kind_macro:
    error('expansion callee must resolve to a macro; found: {}', m)
  pars = m.sig.pars._val
  args = expansion.args._val
  pairs = [] # filled in by mk_cont_bind_pars
  def cont_expand_Macro(val_ignored):
    call_env = StorkEnv(name=m.name._val, code=m, call=expansion, lex=m.env._val, dyn=env,
      tail_depth=0, pairs=pairs)
    return none, mk_cont_list_of_Expr(m.kind.macro._val, 0, call_env, None, 0)
  cont_bind = mk_cont_bind_pars(pars, 0, args, 0, pairs, True, env, cont_expand_Macro)
  return trampoline_loop(none, cont_bind)


def expand_Expansion(expansion, env, is_outer):
  if stork_eval.print_trace:
    errSL(trace_expand_char, expansion)
  expanded = eval_Expansion(expansion, env) # may be only partially expanded.
  if stork_eval.print_trace:
    errSL(trace_post_expand_char, expanded)
  rec_expanded = expand_Expr(expanded, env, False) # fully expand; inner expansions do not assign Src-loc.
  if is_outer:
    adopt_loc(rec_expanded, expansion) # final result gets src-loc of the outermost expansion.
  return rec_expanded


def expand_Array_of_Expr(array, env, is_outer):
  vals = array._val
  exp_vals = [expand_Expr(e, env, True) for e in vals]
  if exp_vals == vals: # no expansion occurred
    return array
  else:
    return mk_Array_of_Expr(exp_vals)


_expand_dispatch = {}


def expand_struct(obj, env, is_outer):
  is_same = True
  items = []
  for type_el in obj._type_of.kind.struct._val:
    n = type_el.name._val
    v = obj[n]
    t = v._type_of
    try:
      f = _expand_dispatch[t]
    except KeyError:
      e = v
    else:
      e = f(v, env, True)
      is_same = is_same and (v is e)
    items.append((n, e))
  return obj if is_same else mk_Obj_with_items(obj._type_of, items)


def expand_to_self(obj, env, is_outer):
  return obj


_expand_dispatch.update({
  Expansion     : expand_Expansion,
  Array_of_Expr : expand_Array_of_Expr,
  SCOPE         : expand_struct,
  LET           : expand_struct,
  IF            : expand_struct,
  FN            : expand_struct,
  Call          : expand_struct,
  Syn_splice    : expand_struct,
  Syn_arg       : expand_struct,
  Syn_seq       : expand_struct,
  Syn_struct    : expand_struct,
  QUO           : expand_to_self,
  Dash          : expand_to_self,
  Sym           : expand_to_self,
  Int           : expand_to_self,
  Str           : expand_to_self,
  Bool          : expand_to_self,
  Accessor      : expand_to_self,
})


def expand_Expr(expr, env, is_outer):
  '''
  is_outer is False only for the case when a macro expansion is the result of a previous expansion.
  it is used to assign the resulting code the Src-loc of the outer expansion.
  '''
  t = expr._type_of
  try:
    f = _expand_dispatch[t]
  except KeyError:
    errL('bad expression type: {}\n  expr: {}', t, expr)
    raise
  return f(expr, env, is_outer)


def expand_eval_loop(expr, env):
  try:
    expanded = expand_Expr(expr, env, True)
  except StorkError as e:
    e.add_env(env)
    raise
  return eval_Expr_loop(expanded, env)


def expand_eval_loop_list(exprs, env):
  '''
  run an eval loop for each expression in the exprs:py_list; return the final value.
  '''
  try:
    val = none
    for expr in exprs:
      val = expand_eval_loop(expr, env)
    return val
  except StorkError as e:
    e.trace(verbose=False)
    raise

