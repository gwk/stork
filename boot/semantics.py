# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

import gloss.string as _string

from syntax import *
from env import *


'''
stork semantic types.
'''


# return type for non-returning functions like error.
Never, never = mk_Type_unit_and_val('Never')


Par = mk_Type_struct('Par', ('name', Str), Type, ('default', Opt_of_Expr), ('is-variad', Bool))
def mk_Par(name, type, default, is_variad):
  return mk_Obj(Par, name=name, type=type, default=default, is_variad=is_variad)


Array_of_Par = derive_Type(Array, El, Par)
def mk_Array_of_Par(py_list):
  return mk_prim(Array_of_Par, py_list)


def mk_Array_of_Par_from_Type_els(type_els):
  l = [mk_Par(e.name, e.type, opt_of_expr_none, false) for e in type_els]
  return mk_Array_of_Par(l)


@memoize
def gen_pars_of_Type(type):
  l = [mk_Par(v.name, Type, mk_Opt_of_Expr(v), false) for v in vars_of_Type(type)]
  return mk_Array_of_Par(l)


Sig = mk_Type_struct('Sig', ('pars', Array_of_Par), ('ret', Type), ('pars-map', Opaque))
def mk_Sig(pars, ret):
  pars_map = mk_Opaque({ p.name._val : p for p in pars._val})
  return mk_Obj(Sig, pars=pars, ret=ret, pars_map=pars_map)


Env = mk_Type_prim('Env', host_type=opaque_env)
def mk_Env(stork_env):
  return mk_prim(Env, stork_env)


Func_kind_host = mk_Type_struct('Func-kind-host', Opaque, ('is-variadic', Bool), ('prim-args', Bool))
def mk_Func_kind_host(py_fn, py_is_variadic, py_prim_args):
  return mk_Obj(Func_kind_host, opaque=mk_Opaque(py_fn), is_variadic=mk_Bool(py_is_variadic), prim_args=mk_Bool(py_prim_args))

Func_kind_interpreted = mk_Type_alias('Func-kind-interpreted', Expr)
Func_kind_macro = mk_Type_alias('Func-kind-macro', Expr)
Func_kind = mk_Type_union('Func-kind', ('host', Func_kind_host), ('interpreted', Func_kind_interpreted), ('macro', Func_kind_macro))
TI_Func_kind_host, TI_Func_kind_interpreted, TI_Func_kind_macro = range(3)

# TODO: make name a Sym? what about the path?
Func = mk_Type_struct('Func', ('name', Str), Env, Sig, ('kind', Func_kind))

def check_Func(func):
  env = func.env
  has_variad = False
  for p in func.sig.pars._val:
    assert p._type_of is Par
    if p.is_variad._val:
      if has_variad:
        error('Func has multiple variadic parameters: {}\n  func: {}', p, func, env=env)
      has_variad = True
    elif has_variad and p.default._tag == TI_Opt_none:
      error('Func parameter following variadic argument must have a default value: {}', p, env=env)

def mk_Func(name, sig, kind, env):
  f = mk_Obj(Func, name=name, env=env, sig=sig, kind=kind)
  check_Func(f)
  return f


Src_file = mk_Type_struct('Src-file', ('name', Str), ('text', Str), ('line-positions', Opaque))
@memoize
def mk_Src_file(py_name, py_text):
  lp = mk_Opaque(_string.line_positions(py_text))
  return mk_Obj(Src_file, name=mk_Str(py_name), text=mk_Str(py_text), line_positions=lp)


Src_loc = mk_Type_struct('Src-loc', ('file', Src_file), ('pos', Int), ('len', Int))
def mk_Src_loc(file, py_pos, py_len):
  return mk_Obj(Src_loc, file=file, pos=mk_Int(py_pos), len=mk_Int(py_len))


semantic_types = [
  Never,
  Par,
  Sig,
  Env,
  Func,
  Src_file,
  Src_loc,
]

other_semantics = [
  never,
]
