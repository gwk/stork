# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

'''
interpreter eval.
'''

import gloss.eh as _eh
from forms import *

trace_eval_char   = '▿' # during trace, printed before each eval; white_down_pointing_small_triangle.
trace_cont_char   = '◦' # during trace, printed before calling continuation; white_bullet.
trace_apply_char  = '▹' # called before each call apply;  white_right_pointing_small_triangle.

print_trace = False


def repr_trunc(v):
  'keep trace printing short so it does not wrap.'
  r = repr(v)
  return r[:128] + '…' if len(r) > 96 else r
      

def check_t(obj, t, msg):
  if not is_Obj_conforming(obj, t):
    error('{}:\n  expected type: {}\n  act: {}\n  obj: {}', msg, t, obj._type_of, obj)


def mk_cont_list_of_Expr(exprs, i, env, cont, td):
  '''
  make a continuation that evals exprs:py_list; the value of the last eval is returned to cont.
  td is tail_depth: the depth of tail recursion for every eval step.
  '''
  li = len(exprs)
  if i == li:
    return cont
  expr = exprs[i]
  ni = i + 1 # next i.
  tdi = td if (ni == li) else -1 # tail depth for expr i.
  def cont_list_of_Expr(val_ignored):
    '''
    val is ignored because each expr in a list is independent.
    a val is returned so that the final value from evaluating the whole list is that of the last expression.
    note that the continuations are created lazily.
    '''
    c = mk_cont_list_of_Expr(exprs, ni, env, cont, td)
    return eval_Expr(expr, env, c, tdi)
  return cont_list_of_Expr


def eval_Array_of_Expr(array, env, cont, td):
  'eval each argument expr in order, returning the last.'
  return none, mk_cont_list_of_Expr(array._val, 0, env, cont, td)


# form evals.


def eval_QUO(quo, env, cont, td):
  'return the quoted syntax.'
  return quo.expr, cont


def eval_SCOPE(scope, env, cont, td):
  'create a new, empty env frame and eval each argument expr in order, returning the last.'
  scope_env = StorkEnv(name='#scope', code=scope, call=scope, lex=env, dyn=env, tail_depth=td)
  return eval_Expr(scope.expr, scope_env, cont, td)


def eval_LET(let, env, cont, td):
  s = let.sym
  if not s._type_of is Sym:
    error('LET: expected a Sym; found: {}', s)
  def cont_LET(val):
    'bind a value to a name in the current env frame.'
    n = s._val
    env.bind_val(n, val)
    return val, cont
  return eval_Expr(let.expr, env, cont_LET, -1)


def eval_IF(if_, env, cont, td):
  p = if_.pred
  t = if_.then
  e = if_['else']
  def cont_IF(val):
    try:
      v = val['-val']
    except KeyError:
      error('IF test requires a primitive value type; received: {}', val)
    if v: # use python bool semantics for now.
      return eval_Expr(t, env, cont, td)
    else:
      return eval_Expr(e, env, cont, td)
  return eval_Expr(p, env, cont_IF, -1)


def mk_Par_from_arg(arg, default_type, env, counter):
  'create a par from a parameter list sequence arg.'
  ta = arg._type_of
  if ta is Sym:
    t = env.resolve(arg._val)
    n = name_from_Type(t, counter)
    d = opt_of_expr_none
    iv = false
  elif ta is Syn_arg:
    n = arg.name
    te = arg.type_expr
    if te._tag == TI_Opt_none:
      t = default_type
    else:
      s = te.a
      check_t(s, Sym, 'parameter list name arg type')
      t = env.resolve(s._val)
    d = arg.val_expr
    iv = arg.is_variad
  else:
    error('parameter must be a Sym, Syn-arg, or Syn-variad; found: {}', arg)
  return mk_Par(name=n, type=t, default=d, is_variad=iv)


def eval_FN(fn, env, cont, td):
  'create a new Func closure.'
  sym = fn.sym
  if not sym._type_of is Sym:
    error('FN: sym must be a Sym; found: {}', sym)
  pars_seq = fn.pars
  if not pars_seq._type_of is Syn_seq:
    error('FN: pars must be a Syn-seq; found: {}', pars_seq)
  ret_sym = fn.ret
  if not ret_sym._type_of is Sym:
    error('FN: ret must be a Sym; found: {}', ret_sym)
  is_macro = fn.is_macro._val
  # name
  name = mk_Str(sym._val)
  is_anon = sym._val.startswith('#') # TODO: improve this hack.
  # pars
  c = Counter()
  default_type = Expr if is_macro else Obj
  pars_list = [mk_Par_from_arg(a, default_type, env, c) for a in pars_seq.exprs._val]
  pars = mk_Array_of_Par(pars_list)
  # ret
  ret = env.resolve(ret_sym._val)
  sig = mk_Sig(pars=pars, ret=ret)
  # body
  e = mk_Env(env)
  k = mk_morph(Func_kind, 'macro' if is_macro else 'interpreted', fn.body)
  f = mk_Func(name=name, env=e, sig=sig, kind=k)
  if not is_anon:
    env.bind_val(sym._val, f)
  adopt_loc(f, fn)
  return f, cont


# call sub-evals


def eval_call_Accessor(call, accessor, args, env, cont, td):
  if len(args) != 1:
    error('accessor call expects a single argument;\n  args: {}', args)
  member_key = accessor._val
  arg_expr = args[0]
  def cont_call_Accessor(val):
    'val is the object resulting from evaluating the arg.'
    # TODO: check that val is a struct or union instance?
    try:
      member = val[member_key]
    except KeyError:
      error('no such member: {}\n obj: {}', member_key, val, env=env)
    else:
      return member, cont
  return eval_Expr(arg_expr, env, cont_call_Accessor, -1)


def mk_cont_par(par, expr, pairs, is_macro, env, cont):
  'eval argument expr and bind to par.'
  is_variad = par.is_variad._val
  assert not expr._type_of is Syn_arg
  def cont_par_bind(val):
    if not is_Obj_conforming(val, par.type):
      error('argument value does not conform to parameter: {}\n  val: {}', par, val, env=env)
    n = par.name._val
    pairs.append(val if is_variad else (n, val))
    return None, cont
  if is_macro:
    def cont_macro_par_eval(val_ignored):
      return expr, cont_par_bind
    return cont_macro_par_eval
  else:
    def cont_par_eval(val_ignored):
      return eval_Expr(expr, env, cont_par_bind, -1)
    return cont_par_eval


def mk_cont_par_variad(pars, pi, args, ai, v_pairs, pairs, is_macro, env, cont):
  'make continuations for evaluating args:py_list and binding to variadic par.'
  par = pars[pi]
  assert par.is_variad._val
  if ai == len(args) or args[ai]._type_of is Syn_arg: # named arg terminates variad.
    c = mk_cont_bind_pars(pars, pi + 1, args, ai, pairs, is_macro, env, cont, has_variad=True)
    array_type = derive_Type(Array, El, par.type)
    def cont_par_variad(val_ignored):
      n = par.name._val
      a = mk_prim(array_type, v_pairs)
      pairs.append((n, a))
      return None, c
    return cont_par_variad
  # some arg
  expr = args[ai]
  if expr._type_of is Syn_splice:
    error('splice arguments not yet implemented.')
  else:
    c = mk_cont_par_variad(pars, pi, args, ai + 1, v_pairs, pairs, is_macro, env, cont)
    return mk_cont_par(par, expr, v_pairs, is_macro, env, c)


def mk_cont_bind_pars(pars, pi, args, ai, pairs, is_macro, env, cont, has_variad=False):
  '''
  make continuations that eval args:py_list and bind the results to the elements of pars:py_list.
  the resulting (name, value) pairs are appended to pairs:py_list.
  pi is the current index into pars.
  ai is the current index into args.
  '''
  assert isinstance(pars, list)
  assert isinstance(args, list)
  if pi == len(pars): # terminal case.
    if ai < len(args): # not all arguments have been consumed.
      error('excessive or mismatched arguments: {};\n  parameters: {}\n  args: {}', args[ai], pars, args)
    return cont
  par = pars[pi]
  if par.is_variad._val:
    assert not has_variad # should have been caught by check_Func.
    return mk_cont_par_variad(pars, pi, args, ai, [], pairs, is_macro, env, cont)
  # non-variadic par; inspect arg.
  if ai == len(args): # arguments have been exhausted.
    if par.default._tag == TI_Opt_none:
      error('insufficient arguments for parameters: {}', pars)
    else:
      error('default arguments not supported')
  # some arg (polymorphic).
  arg = args[ai]
  at = arg._type_of
  if at is Syn_arg:
    arg_name = arg.name._val
    if arg_name != par.name._val:
      if par.default_expr._tag == TI_Opt_none:
        error('argument name does not match parameter: {};\n arg: {}', par, arg)
      else: # use the default; try to match this arg to a subsequent par.
        error('default arguments not yet supported')
    if arg.is_variad._val:
      error('unexpected variad syntax in arguments: {}', arg)
    if arg.type_expr._tag != TI_Opt_none:
      error('named argument has a type expression: {}', arg)
    if arg.val_expr._tag == TI_Opt_none:
      error('named argument has no value expression: {}', arg)
    expr = arg.val_expr.a
  elif at is Syn_splice:
    error('splice arguments not yet implemented.')
  else:
    expr = arg
  c = mk_cont_bind_pars(pars, pi + 1, args, ai + 1, pairs, is_macro, env, cont, has_variad=has_variad)
  return mk_cont_par(par, expr, pairs, is_macro, env, c)


def eval_call_Func(call, func, args, env, cont, td):
  kind = func.kind
  sig = func.sig
  pars = sig.pars._val
  ret = sig.ret
  kt = kind._tag
  pairs = [] # filled in by mk_cont_bind_pars
  if kt == TI_Func_kind_host:
    host_f = kind.host.opaque._val
    takes_prim_args = kind.host.prim_args._val
    def cont_call_Func_host(val_ignored):
      try:
        host_args = [v._val for k, v in pairs] if takes_prim_args else [v for k, v in pairs]
        host_val = host_f(*host_args)
      except StorkError as e:
        e.add_env(env)
        raise e
      except Exception as e:
        error('host call failed: {}; {}: {}', host_f, type(e).__name__, e, env=env)
      if isinstance(host_val, Stork):
        t_act = host_val._type_of
        if not is_Type_conforming(t_act, ret):
          error('host call expected return type: {}\n  actual: {}', ret, t_act)
        val = host_val
      else: # package host values in the appropriate primitive types.
        val = none if ret is None_ else mk_prim(ret, host_val)
      return val, cont
    c = cont_call_Func_host

  elif kt == TI_Func_kind_interpreted:
    tdc = td + 1
    def cont_call_Func_interpreted(val_ignored):
      call_env = StorkEnv(name=func.name._val, code=func, call=call, lex=func.env._val, dyn=env,
        tail_depth=tdc, pairs=pairs)
      expr = kind.interpreted
      return eval_Expr(expr, call_env, cont, tdc)
    c = cont_call_Func_interpreted

  elif kt == TI_Func_kind_macro:
    error('cannot call macro: {}', func)

  else:
    error('cannot call Func-kind: {}', kind)

  return None, mk_cont_bind_pars(pars, 0, args, 0, pairs, False, env, c)


_reified_types = {}

def eval_reify_Type(t, pars, args, env, cont):
  if not args:
    return t, cont
  pairs = [] # filled in by mk_cont_bind_pars
  def cont_reify_Type(val_ignored):
    try:
      dt = t
      for name, type_val in pairs:
        type_var = mk_Type_var(name) # memoized, so this works.
        if type_val.kind._tag == TI_var:
          error('substituting one type var for another:\n  var: {}\n  val: {}', type_var, type_val)
        dt = derive_Type(dt, type_var, type_val)
      if dt == t:
        error('empty derivation: {}', t)
      return dt, cont
    except StorkError as e:
      e.add_env(env)
      raise
  return None, mk_cont_bind_pars(pars._val, 0, args, 0, pairs, False, env, cont_reify_Type)


@memoize
def call_type_pars(t):
  k = t.kind
  kt = k._tag
  if kt is TI_prim_gen:
    el_type = k.prim_gen.type
    par = mk_Par(name=mk_Str('items'), type=el_type, default=opt_of_expr_none, is_variad=true)
    return mk_Array_of_Par([par])
  if kt is TI_struct:
    type_els = k.struct._val
    return mk_Array_of_Par_from_Type_els(type_els)
  if kt is TI_union:
    error('union constructors not yet mplemented.')
  error('cannot call type as constructor: {}', t)


def eval_call_Type(call, t, args, env, cont, td):
  generic_pars = gen_pars_of_Type(t)
  if generic_pars._val: # non-empty; type is generic.
    if not args:
      error('cannot reify generic type without an argument: {}', t)
    return eval_reify_Type(t, generic_pars, args, env, cont)
  # type is concrete.
  pars = call_type_pars(t)
  pairs = [] # filled in by mk_cont_bind_pars
  def cont_call_Type_struct(val_ignored):
    k = t.kind
    if k._tag == TI_prim_gen:
      py_type = k.prim_gen.host_type._val
      if py_type is dict:
        error('dict constructor not yet supported.', env=env)
      return mk_Obj(t, _val=py_type(pairs[0][1]._val)), cont
    assert all(not par.is_variad._val for par in pars._val)
    return mk_Obj_with_items(t, pairs), cont
  return None, mk_cont_bind_pars(pars._val, 0, args, 0, pairs, False, env, cont_call_Type_struct)

 

eval_call_dispatch = {
  Accessor  : eval_call_Accessor,
  Func      : eval_call_Func,
  Type      : eval_call_Type,
}

def eval_Call(call, env, cont, td):
  callee = call.callee
  args = call.args._val

  def cont_call_choose_application(val_callee):
    'choose an application behavior based on callee type.'
    t = val_callee._type_of
    try:
      f = eval_call_dispatch[t]
    except KeyError:
      error('value is not callable: {}\nvalue type: {}', val_callee, t, env=env)
    try:
      if print_trace:
        errS(trace_apply_char, repr_trunc(val_callee))
        errL(' td={}'.format(td) if td > 0 else '')
      return f(call, val_callee, args, env, cont, td)
    except StorkError as e:
      e.add_env(env)
      raise

  def cont_call(val_ignored):
    'eval the callee expression.'
    return eval_Expr(callee, env, cont_call_choose_application, -1)
  return None, cont_call


def mk_cont_Eval_seq(exprs, i, vals, env, cont):
  '''
  create chained continuations to evaluate each element of exprs:py_list;
  store the results in vals:py_list, which must be provided by initial call.
  '''
  if i == len(exprs):
    return cont
  c = mk_cont_Eval_seq(exprs, i + 1, vals, env, cont)
  expr = exprs[i]
  def cont_Eval_seq_append(val):
    vals.append(val)
    return None, c
  def cont_Eval_seq_el(val_ignored):
    return eval_Expr(expr, env, cont_Eval_seq_append, -1)
  return cont_Eval_seq_el


def eval_Syn_seq(syn_seq, env, cont, td):
  'TODO: reconsider. Does this encourage proliferation of weakly typed (Array Obj)?'
  exprs = syn_seq.exprs._val
  vals = []
  def cont_Syn_seq(val_ignored):
    return mk_Array_of_Obj(vals), cont
  return None, mk_cont_Eval_seq(exprs, 0, vals, env, cont_Syn_seq)


def eval_Syn_struct(syn_struct, env, cont, td):
  raise Exception # TODO



def eval_Sym(sym, env, cont, td):
  key = sym._val
  val = env.resolve(key)
  return val, cont


def eval_to_self(expr, env, cont, td):
  return expr, cont


# eval dispatch table
eval_fns = {
  Array_of_Expr : eval_Array_of_Expr,
  QUO           : eval_QUO,
  SCOPE         : eval_SCOPE,
  LET           : eval_LET,
  IF            : eval_IF,
  FN            : eval_FN,
  Call          : eval_Call,
  Syn_seq       : eval_Syn_seq,
  Syn_struct    : eval_Syn_struct,
  Sym           : eval_Sym,
  Int           : eval_to_self, 
  Str           : eval_to_self,
  Bool          : eval_to_self,
  Accessor      : eval_to_self,
}


expr_types = set(eval_fns.keys())


def eval_Expr(expr, env, cont, td):
  '''
  top level eval.
  dispatches to the appropriate eval function based on expr type.
  returns a (value, continuation) pair.
  '''
  if print_trace:
    errSL(trace_eval_char, repr_trunc(expr))
  _eh.check_type(expr, Stork)
  t = expr._type_of
  try:
    f = eval_fns[t]
  except KeyError:
    error('cannot evaluate expr: {}', expr, env=env)
  try:
    return f(expr, env, cont, td)
  except StorkError as e:
    e.add_env(env)
    raise


def trampoline_loop(val, cont):
  '''
  trampoline loop over the current continuation; each iteration runs a step of the computation.
  '''
  while cont:
    if print_trace:
      errSL(trace_cont_char, cont.__name__, repr_trunc(val))
    val, cont = cont(val)
  return val


def eval_Expr_loop(expr, env):
  return trampoline_loop(*eval_Expr(expr, env, None, -1))

