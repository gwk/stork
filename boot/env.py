# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

import gloss.string as _string

from gloss.io.sgr import sgr, R, GD
from gloss.io.std import *


src_locs = {} # populated by boot/loc


def src_loc_str(src_loc):
  f = src_loc.file
  t = f.text._val
  lp = f.line_positions._val
  line, row, col = _string.highlight_line_row_col(t, lp, src_loc.pos._val, src_loc.len._val, ul=False)
  return '{n}: {r}:{c}\n  {l}\n'.format(n=f.name._val, r=row + 1, c=col + 1, l=line)


def src_loc_str_for_code(code):
  try:
    loc = src_locs[code]
  except KeyError:
    return '<NO SOURCE: {}>\n'.format(code)
  return src_loc_str(loc)


def adopt_loc(new, existing):
  'give new:Expr the Src-loc of existing:Expr.'
  try:
    loc = src_locs[existing]
  except KeyError:
    return
  src_locs.setdefault(new, loc)


class StorkError(Exception):
  
  def __init__(self, format, *items, env=None):
    'env is optional; some eval functions will catch and add it if it is missing.'
    super().__init__(format.format(*items))
    self.env = env

  def trace(self, verbose):
    if self.env is None:
      raise ValueError('stork error has no associated env; must be specified in the call to error().')
    errL('\nlexical trace:')
    self.env.trace_lex(verbose)
    errL('\ndynamic trace:')
    self.env.trace_dyn(verbose)
    errSL('\nerror:', self)

  def add_env(self, env):
    'add the current env if it was not already specified.'
    if not self.env:
      self.env = env


def error(format, *items, env=None):
  raise StorkError(format, *items, env=env)


class StorkEnv():

  max_tail_depth = 8

  def __init__(self, name, code=None, call=None, lex=None, dyn=None, tail_depth=-1, pairs=()):
    self.name = name
    self.code = code
    self.call = call
    self.lex = lex
    self.tail_depth = tail_depth
    # elide frames if we are tail-recursing deeply.
    if dyn and tail_depth > self.__class__.max_tail_depth:
      self.dyn = dyn.dyn
    else:
      self.dyn = dyn
    self.bindings = {}
    for p in pairs:
      self.bind_val(*p)


  def __repr__(self):
    return '<StorkEnv {}>'.format(self.name)

  
  def bind_val(self, name, val):
    if name in self.bindings:
      error("name already bound: {}", name, env=self)
    self.bindings[name] = val


  def resolve(self, name): # TODO: take a Sym/Str as arg, check and unpack _val here?
    e = self
    while e:
      try:
        return e.bindings[name]
      except KeyError:
        e = e.lex
    error('unbound name: {}', name, env=self)


  def trace_lex(self, verbose):
    if self.lex:
      self.lex.trace_lex(verbose)
    if self.code:
      err(src_loc_str_for_code(self.code))
    elif self.name:
      errL(self.name)
    else:
      errL('<UNKNOWN>')
    if verbose:
      errSL('   ', *sorted(self.bindings.keys()))

  def trace_dyn(self, verbose):
    td = self.tail_depth
    p = self.dyn
    if p:
      ptd = p.tail_depth
      p.trace_dyn(verbose)
      if ptd < td - 1:
        errFL('(elided {} tail frames)', td - ptd)
    if self.call:
      err(src_loc_str_for_code(self.call))
    elif self.name:
      errL(self.name)
    else:
      errL('<UNKNOWN>')
    if verbose:
      errSL('   ', *sorted(self.bindings.keys()))

