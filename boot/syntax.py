# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

from stork_types import *

'''
stork syntax types.
'''


Expr = mk_Type_class('Expr') # untyped for now; should be a closed Type-set.

Opt_of_Expr = derive_Type(Opt, El, Expr)
opt_of_expr_none = mk_morph(Opt_of_Expr, 'none', none)
def mk_Opt_of_Expr(e):
  'translate python false values into the None morph.'
  return mk_morph(Opt_of_Expr, 'a', e) if e else opt_of_expr_none


Array_of_Expr = derive_Type(Array, El, Expr)
def mk_Array_of_Expr(py_list):
  return mk_prim(Array_of_Expr, py_list)


Sym = mk_Type_prim('Sym', host_type=opaque_str)
def mk_Sym(val):
  return mk_prim(Sym, val)

repr_dispatch[Sym] = lambda o: '`' + o['-val']


Accessor = mk_Type_prim('Accessor', host_type=opaque_str)
def mk_Accessor(val):
  return mk_prim(Accessor, val)


Dash, dash = mk_Type_unit_and_val('Dash')


Syn_splice = mk_Type_struct('Syn-splice', ('expr', Expr))
def mk_Syn_splice(expr):
  return mk_Obj(Syn_splice, expr=expr)


# TODO: rename to Syn-label?
Syn_arg = mk_Type_struct('Syn-arg', ('name', Str), ('type-expr', Opt_of_Expr), ('val-expr', Opt_of_Expr), ('is-variad', Bool))
def mk_Syn_arg(py_name, type_expr, val_expr, is_variad):
  return mk_Obj(Syn_arg, name=mk_Str(py_name), type_expr=type_expr, val_expr=val_expr, is_variad=is_variad)


# Syn-seq is not simply an alias of (Array -el=Expr) because the latter should not have an eval rule.
Syn_seq = mk_Type_struct('Syn-seq', ('exprs', Array_of_Expr))
def mk_Syn_seq(exprs):
  return mk_Obj(Syn_seq, exprs=exprs)


Syn_struct = mk_Type_struct('Syn-struct')
def mk_Struct(a):
  error('Struct unimplemented.')


Call = mk_Type_struct('Call', ('callee', Expr), ('args', Array_of_Expr))
def mk_Call(callee, args):
  return mk_Obj(Call, callee=callee, args=args)


Expansion = mk_Type_struct('Expansion', ('callee', Expr), ('args', Array_of_Expr))
def mk_Expansion(callee, args):
  return mk_Obj(Expansion, callee=callee, args=args)


syntax_types = [
  Expr,
  Syn_arg,
  Syn_seq,
  Syn_struct,
  Call,
  Expansion,
]

# for checking
other_syntax = [
  Opt_of_Expr,
  opt_of_expr_none,
]
