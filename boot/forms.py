# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

'''
builtin special forms.
'''

from semantics import *


QUO   = mk_Type_struct('QUO', Expr)
UNQ   = mk_Type_struct('UNQ', Expr)
SCOPE = mk_Type_struct('SCOPE', Expr)
IF    = mk_Type_struct('IF', ('pred', Expr), ('then', Expr), ('else', Expr))
LET   = mk_Type_struct('LET', ('sym', Expr), ('expr', Expr))
FN    = mk_Type_struct('FN', ('sym', Expr), ('pars', Expr), ('ret', Expr), ('body', Expr), ('is-macro', Bool))

all_forms = [QUO, UNQ, SCOPE, IF, LET, FN]
