# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

import gloss.eh as _eh
from env import *
from semantics import *

'''
various runtime checks.
'''

# instances of these kinds are expected to have a fixed set of keys.
unit_keys   = { '-type-of' }
prim_keys   = { '-type-of', '-val' }
vec_keys    = { '-type-of', '-vals' }

# each check function performs kind-specific checks.
# they return a pair of values:
#  a set of expected keys;
#  a sequence of stork objects that obj refers to.
# obj._type_of is passed separately for convenience.


def check_prim(t, obj):
  v = obj._val
  host_t_exp = t.kind.prim.host_type._val
  _eh.checkF(isinstance(v, host_t_exp), ValueError,
    'bad prim val: {}; expected python type: {}\n  type: {}\n  obj: {}',
    v, host_t_exp, t, obj)
  return prim_keys, ()


def check_py_seq(el_t_exp, s):
  for el in s:
    el_t_act = el._type_of
    _eh.checkF(is_Type_conforming(el_t_act, el_t_exp), ValueError,
    'bad Array element: {}\n  exp: {}\n  act: {}\n  seq: {}', el, el_t_exp, el_t_act, s)


def check_py_dict(el_type, d):
  raise NotImplementedError


_check_py_dispatch = {
  list  : check_py_seq,
  set   : check_py_seq,
  dict  : check_py_dict,
}


def check_prim_gen(t, obj):
  v = obj._val
  host_t = t.kind.prim_gen.host_type._val
  _eh.checkF(isinstance(v, host_t), ValueError,
    'bad prim-gen val: {}; expected python type: {}\n  type: {}\n  obj: {}',
    v, host_t, t, obj)
  _check_py_dispatch[host_t](t.kind.prim_gen.type, v)
  return prim_keys, ()


def check_unit(t, obj):
  return unit_keys, ()


def check_union(t, obj):
  variants = t.kind.union._val # py list
  tag_index = obj._tag
  variant = variants[tag_index] # O-n list access; returns Type_el.
  name = variant.name._val # str
  member = obj[name]
  #errSL('check_union:', variants, '\n', tag_index, '\n', variant)
  vt_exp = resolve_Type_alias(variant.type)
  vt_act = member._type_of
  _eh.checkF(is_Type_conforming(vt_act, vt_exp), ValueError,
    'bad morph member:\n  exp: {}\n  act: {}\n  type: {}\n  obj: {}', vt_exp, vt_act, t, obj)
  union_keys = { '-type-of', '-tag', name }
  return union_keys, (member,)


def check_struct(t, obj):
  fields = t.kind.struct._val # py list
  struct_keys = { '-type-of' }
  for f in fields:
    n = f.name._val
    struct_keys.add(n)
    ft_exp = resolve_Type_alias(f.type)
    try:
      ft_act = obj[n]._type_of
    except KeyError:
      continue # will get caught by set comparison in check_obj.
    _eh.checkF(is_Type_conforming(ft_act, ft_exp), ValueError,
      'bad struct field: {}\n  exp: {}\n  act: {}\n  type: {}\n  obj: {}',
      n, ft_exp, ft_act, t, obj)
  return struct_keys, obj.values()


def check_vec(t, obj):
  # TODO: check length and element types.
  return vec_keys, obj._vals


kind_checks = {
  TI_unit     : check_unit,
  TI_prim     : check_prim,
  TI_prim_gen : check_prim_gen,
  TI_union    : check_union,
  TI_struct   : check_struct,
  TI_vec      : check_vec,
}


def check_obj(obj):
  "check an object's shallow structure against its type."
  #errSL('check_obj:', obj)
  try:
    t = obj._type_of
    kind_tag = t.kind._tag
    keys_exp, stork_vals = kind_checks[kind_tag](t, obj)
    keys_act = set(obj.keys())
    _eh.checkF(keys_act == keys_exp, ValueError,
      'bad keys for {};\n  exp: {}\n  act: {}\n  kind: {}\n  obj: {}',
      t.name._val, sorted(keys_exp), sorted(keys_act), t.kind, obj)
    return stork_vals, t
  except Exception:
    errL('check failed for obj:')
    for k, v in sorted(obj.items()):
      errFL('  {}: {}', k, v)
    raise


obj_set = set()

def check_graph(label, objects_iter):
  errSLI('check_graph:', label)
  objects = list(objects_iter)
  while objects:
    obj = objects.pop()
    if obj not in obj_set:
      stork_vals, t = check_obj(obj)
      obj_set.add(obj)
      for v in stork_vals:
        if v not in obj_set:
          objects.append(v)
      if t not in obj_set:
         objects.append(t)

