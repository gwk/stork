#!/usr/bin/env python3
# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

'''
bootstrap interpreter.
'''

import sys as _sys
import ast as _ast

import gloss.arg as _arg
import gloss.comb.comb as _comb
import gloss.path as _path
import gloss.string as _string

from gloss.io.std import *


args = _arg.parse(
  'stork bootstrap interpreter.',
  _arg.flag('-trace', help='write messages to std err tracing the execution of the interpreter.'),
  _arg.flag('-safe', help='extra runtime checks.'),
  _arg.flag('-interactive', help='always enter interactive environment.'),
  _arg.Pattern('-commands', nargs='+', help='execute commands.'),
  _arg.Pattern('paths', nargs='+', help='source file paths.'),
)

# import stork modules after arg parsing to enable debug printing.
# need these module handles to set various globals.
import stork_eval
import stork_types
from host_functions import *
from stork_expand import *
import stork_check as _check

# perform checks
_check.check_graph('debug', [])
_check.check_graph('Type', [Type])
_check.check_graph('types', type_types)
_check.check_graph('derived types', derived_types)
_check.check_graph('type instances', (i for n, i in type_instances))
_check.check_graph('syntax types', syntax_types)
_check.check_graph('other syntax', other_syntax)
_check.check_graph('semantic types', semantic_types)
_check.check_graph('other semantics', other_semantics)
_check.check_graph('forms', all_forms)
_check.check_graph('host functions', host_functions)

# options setup.
stork_eval.print_trace = args.trace
should_run_interactive = args.interactive or not (args.paths or args.commands)

if args.safe:
  stork_types.mk_Obj_check = _check.check_obj

# each gen function takes arguments (parser, match, subs).
# where subs are the gen results for each sub-match;
# if a gen function is not supplied a KeyedList (similar to associative array) is generated.

def loc(p, m, e):
  'save source location information from Match m for Expr e.'
  sf = mk_Src_file(p.name, p.string)
  sl = mk_Src_loc(sf, m.pos, m.size)
  assert sl not in src_locs
  src_locs[e] = sl
  return e


def gen_string(p, m, s):
  choice_match = m.subs[0]
  literal_match = choice_match.sub
  literal = literal_match.val(p.string)
  try:
    string = _ast.literal_eval(literal)
  except SyntaxError:
    errSL('syntax error in string literal:', repr(literal))
    raise
  return loc(p, m, mk_Str(string))


def gen_path(p, m, s):
  'for now, just use the path string and return a Sym object for simplicity.'
  # this is how to get the dirs and name from the match, but ignore them for now.
  #dirs = s[0].list
  #name = s[1][0]
  string = m.val(p.string)
  return mk_Sym(string)


def gen_arg(p, m, s, is_variad):
  return loc(p, m, mk_Syn_arg(
    s['name'], mk_Opt_of_Expr(s[3]),  mk_Opt_of_Expr(s[4]), mk_Bool(is_variad)))


gen_functions = {
  's' : None, # for debug clarity
  'name'      : lambda p, m, s: m.val(p.string),
  'dir'       : lambda p, m, s: s['name'],
  'dirs'      : lambda p, m, s: s.list,
  'number'    : lambda p, m, s: loc(p, m, mk_Int(int(m.subs[0].val(p.string)))),
  'sym'       : lambda p, m, s: loc(p, m, mk_Sym(s['name'])),
  'path'      : gen_path,
  'string'    : gen_string,
  'accessor'  : lambda p, m, s: loc(p, m, mk_Accessor(s[1])),
  'dash'      : lambda p, m, s: dash,
  'ann-type'  : lambda p, m, s: s['expr'],
  'ann-val'   : lambda p, m, s: s['expr'],
  'splice'    : lambda p, m, s: loc(p, m, mk_Syn_splice(s['expr'])),
  'arg'       : lambda p, m, s: gen_arg(p, m, s, False),
  'variad'    : lambda p, m, s: gen_arg(p, m, s, True),
  'seq'       : lambda p, m, s: loc(p, m, mk_Syn_seq(s['exprs'])),
  'struct'    : lambda p, m, s: loc(p, m, mk_Struct(s['exprs'])),
  'call'      : lambda p, m, s: loc(p, m, mk_Call(s['expr'], s['exprs'])),
  'expansion' : lambda p, m, s: loc(p, m, mk_Expansion(s['expr'], s['exprs'])),
  'expr'      : lambda p, m, s: s[0],
  'exprs'     : lambda p, m, s: loc(p, m, mk_Array_of_Expr(s[1].list)),
}

boot_dir  = _path.dir(_sys.argv[0])
root_dir  = _path.join(boot_dir, '..')
src_dir   = _path.join(root_dir, 'src')
grammar_path = _path.join(boot_dir, 'stork.cg')
core_path = _path.join(src_dir, 'core.stork')

push_limit_vol_err('note') # don't print grammar parsing unless debug
grammar = _comb.parse_grammar_from_path(grammar_path)
pop_vol_err()

_comb.errI_grammar(grammar)
start_rule   = grammar['exprs']
errSLD('start rule:', start_rule)

# create global environment
global_pairs = []
all_builtin_types = type_types + syntax_types + semantic_types + all_forms
global_pairs.extend((T.name._val, T) for T in all_builtin_types)
global_pairs.extend(type_instances)
global_pairs.extend((f.name._val, f) for f in host_functions)
global_env = StorkEnv(name='#global', pairs=global_pairs)


def handle_parser(parser, env):
  errLI('\n', parser.name, ' match:')
  errRI(parser.match, context=parser.string)
  errLI('\n', parser.name, ' gen:')
  code = parser.generate(gen_functions)
  errRI(code, depth_max=20)
  errLI()
  assert code._type_of is Array_of_Expr
  return expand_eval_loop_list(code._val, env)


def parse_str(s, rule_name=start_rule.name):
  'parse s using the specified rule and return the resulting Stork Expr.'
  parser = _comb.parse_string(rule_name, s, grammar[rule_name])
  assert parser.match
  code = parser.generate(gen_functions)
  return code


def def_macro_macro():
  '''
  assemble the 'macro' macro manually and add to global_env.
  the definition is equivalent to:
    (macro macro <pars_str> <body_str>)
  '''
  pars_str = '[-sym -pars &body]'
  body_str = '(FN -sym=sym -pars=pars -ret=<quo Expr> -body=body -is-macro=true)'
  sym = mk_Sym('macro')
  pars = parse_str(pars_str, 'seq')
  body = parse_str(body_str, 'exprs')
  # since quo is not yet defined, perform the quotation by hand.
  e = mk_Sym('Expr')
  rve = body._val[0].args._val[2].val_expr # ret val-expr
  assert rve._type_of is Opt_of_Expr
  rve['a'] = mk_Obj(QUO, expr=e)
  macro_fn = mk_Obj(FN, sym=sym, pars=pars, ret=e, body=body, is_macro=true)
  expand_eval_loop(macro_fn, global_env)

def_macro_macro()


def run_path(path, env):
  'execute code frome source file at path in env.'
  try:
    errSLI('src_path:', path)
    parser = _comb.parse_file_at_path(path, start_rule)
    return handle_parser(parser, env)
  except StorkError:
    _sys.exit(1)


def run_paths(base_env):
  '''
  execute code in paths specified on command line.
  returns either the #files env in which all files were evaluated,
  or else the provided base env if paths is empty.
  '''
  # iterate over source paths
  if not args.paths:
    return base_env
  env = StorkEnv(name='#files', lex=base_env)
  for path in args.all_paths(exts=['.stork']):
    run_path(path, env)
  return env


rep_prompt          = '▷ ' # white_right_pointing_triangle
rep_continue_prompt = '… ' # ellipsis
rep_val_prompt      = '○ ' # white_circle

def run_commands(base_env):
  'execute code in source specified on command line (-commands flag).'
  try:
    source = ' '.join(args.commands)
    if not source.endswith('\n'):
      source += '\n'
    parser = _comb.parse_string('#commands', source, start_rule)
    env = StorkEnv(name=parser.name, lex=base_env)
    if parser.match:
      res = handle_parser(parser, env)
      if res:
        outSL(rep_val_prompt, res)
    return env
  except StorkError:
    _sys.exit(1)


def run_interactive(base_env):
  'enter the interactive read-eval-print loop (REPL).'
  lines = []
  env = StorkEnv(name='#interactive', lex=base_env)
  while True:
    try:
      prompt = rep_continue_prompt if lines else rep_prompt
      line = input(prompt)
      lines.append(line + '\n')
      source = ''.join(lines)
      parser = _comb.parse_string(env.name, source, start_rule)
      if parser.match:
        del lines[:] # clear line buffer.
        res = handle_parser(parser, env)
        if res:
          outL(rep_val_prompt, res)
    except (StorkError, _comb.ParseError):
      continue
    except KeyboardInterrupt:
      del lines[:] # ctrl-c clears line buffers.
      errL(' Interrupt')
      continue
    except EOFError:
      errL(' EOF')
      return

core_env = StorkEnv(name='#core', lex=global_env)
run_path(core_path, core_env)
env = run_paths(core_env)
if args.commands:
  env = run_commands(env)
if should_run_interactive:
  run_interactive(env)

