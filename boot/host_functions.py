# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

import operator as _op
from collections import Counter as _Counter
from sys import exit as _exit, stdout as _stdout, stderr as _stderr

from env import *
from semantics import *

'''
builtin functions that wrap python host functions.
'''

def _type_of(obj): return obj._type_of

def _array_el(l, i):  return l[i]
def _array_hd(l):     return l[0]

def _array_slice_from(a, i):
  return mk_Obj(a._type_of, _val=a._val[i._val:])

def _array_slice_to(a, i):
  return mk_Obj(a._type_of, _val=a._val[:i._val])

def _array_slice(a, i, j):
  return mk_Obj(a._type_of, _val=a._val[i._val:j._val])


def _dict_get(d, k): return d[k]


def _out(l):    print(*l, sep='',   end='');                  return l[-1] if l else none;
def _outS(l):   print(*l, sep=' ',  end='');                  return l[-1] if l else none;
def _outL(l):   print(*l, sep='',   end='\n');                return l[-1] if l else none;
def _outSS(l):  print(*l, sep=' ',  end=' ');                 return l[-1] if l else none;
def _outSL(l):  print(*l, sep=' ',  end='\n');                return l[-1] if l else none;
def _outLL(l):  print(*l, sep='\n', end='\n');                return l[-1] if l else none;
def _err(l):    print(*l, sep='',   end='',   file=_stderr);  return l[-1] if l else none;
def _errS(l):   print(*l, sep=' ',  end='',   file=_stderr);  return l[-1] if l else none;
def _errL(l):   print(*l, sep='',   end='\n', file=_stderr);  return l[-1] if l else none;
def _errSS(l):  print(*l, sep=' ',  end=' ',  file=_stderr);  return l[-1] if l else none;
def _errSL(l):  print(*l, sep=' ',  end='\n', file=_stderr);  return l[-1] if l else none;
def _errLL(l):  print(*l, sep='\n', end='\n', file=_stderr);  return l[-1] if l else none;

def _flush_out(): _stdout.flush()
def _flush_err(): _stderr.flush()

def _errorF(format, l):
  error(format, *l)


host_env = mk_Env(StorkEnv('#host')) # dummy env for host functions, which never use it.

host_functions = []

def _f(name, py_fn, py_pars, ret, v=None, p=True):
  'register a host (builtin) function. py_pars is a py seq of stork types.'
  h = mk_Func_kind_host(py_fn, py_is_variadic=False, py_prim_args=p)
  k = mk_morph(Func_kind, 'host', h)
  c = _Counter()
  # auto name each parameter by type name.
  pars_list = [mk_Par(*auto_name_Type(t, c), default=opt_of_expr_none, is_variad=false) for t in py_pars]
  if v: # add a variadic argument
    pars_list.append(mk_Par(name=mk_Str('items'), type=v, default=opt_of_expr_none, is_variad=true))
  pars = mk_Array_of_Par(pars_list)
  sig = mk_Sig(pars, ret)
  f = mk_Func(name=mk_Str(name), env=host_env, sig=sig, kind=k)
  host_functions.append(f)

_f('add', _op.add, (Int, Int), Int)
_f('sub', _op.sub, (Int, Int), Int)
_f('mul', _op.mul, (Int, Int), Int)
_f('divi', _op.floordiv,  (Int, Int), Int)
_f('mod', _op.mod, (Int, Int), Int)
_f('neg', _op.neg, (Int,), Int)
_f('abs', _op.abs, (Int,), Int)
_f('pow', pow, (Int, Int), Int)
_f('eq', _op.eq, (Int, Int), Bool)
_f('lt', _op.lt, (Int, Int), Bool)
_f('le', _op.le, (Int, Int), Bool)
_f('gt', _op.gt, (Int, Int), Bool)
_f('ge', _op.ge, (Int, Int), Bool)

_f('not', _op.not_, (Bool,), Bool)

_f('exit', exit, (Int,), None_)

_f('type-of', _type_of, (Obj,), Type, p=False)

# array functions.
# TODO: strong typing for generic functions.
_f('_array-len', len, (('array', Obj),), Int)
_f('_array-el', _array_el, (('array', Obj), ('index', Int)), Obj)
_f('_array-hd', _array_hd, (('array', Obj),), Obj)
_f('_array-slice-from', _array_slice_from, (('array', Obj), ('from', Int)), Obj, p=False)
_f('_array-slice-to', _array_slice_to, (('array', Obj), ('to', Int)), Obj, p=False)
_f('_array-slice', _array_slice, (('array', Obj), ('from', Int), ('to', Int)), Obj, p=False)


# printing functions.
_f('out',   _out,    (), Obj, v=Obj)
_f('outS',  _outS,   (), Obj, v=Obj)
_f('outL',  _outL,   (), Obj, v=Obj)
_f('outSS', _outSS,  (), Obj, v=Obj)
_f('outSL', _outSL,  (), Obj, v=Obj)
_f('outLL', _outLL,  (), Obj, v=Obj)
_f('err',   _err,    (), Obj, v=Obj)
_f('errS',  _errS,   (), Obj, v=Obj)
_f('errL',  _errL,   (), Obj, v=Obj)
_f('errSS', _errSS,  (), Obj, v=Obj)
_f('errSL', _errSL,  (), Obj, v=Obj)
_f('errLL', _errLL,  (), Obj, v=Obj)

_f('flush-out', _flush_out, (), None_)
_f('flush-err', _flush_err, (), None_)

_f('errorF', _errorF, (Str,), Never, v=Obj)

