# Copyright 2013 George King.
# Permission to use this file is granted in stork/license.txt.

import gloss.eh as _eh
from collections import Counter
from gloss.decorators import memoize, memoize_rec
from gloss.containers import HashableDict
from gloss.string import ellipse_repr
from env import *

'''
Stork type system.
All types in stork are first-class objects that describe the layout of information in other objects.
Type is a singleton that is its own type, and describes the layout of all types.
See doc/types.txt for details.

Because the Type singleton introduces a variety of circular references,
the objects involved are constructed incrementally by hand.
'''


# dispatch table to specialize _repr; filled in later.
repr_dispatch = {}
_repr_excluded_tags = ('-type-of', '-tag')

class Stork(HashableDict):
  '''
  all stork runtime objects are python Stork instances.
  This dict subclass uses identity rather than equality for comparison (same as objects).
  It also has a more legible desciption functions, reducing circular printing due to type-of.
  Each instance must contain a Type under the special key '-type-of'.
  The remaining items in the instance must match the information in the type definition.
  Prim and Prim-gen types have a single key '-val'.
  Struct type instances have keys matching their field names;
  Union types have a '-tag' key containing the tag index, plus a single member keyed by name.
  '''

  def desc(self, context=None):
    # do not use attr access here because it can cause infinite loops during error reporting.
    return self['-type-of']['name']['-val']

  def descChildren(self, context, sort_unordered):
    items = self.items()
    s = sorted(self.items()) if sort_unordered else items
    return (p for p in s if p[0] != '-type-of')

  def _repr(self, depth, obj_set):
    t = self['-type-of']
    try:
      f = repr_dispatch[t]
    except KeyError:
      pass
    else:
      return f(self)
    if self in obj_set:
      return '↺' # anticlockwise gapped circle arrow
    if depth == 0:
      return '…' # ellipsis
    d = depth - 1
    obj_set.add(self)
    tnv = t['name']['-val']
    try: # prim-gen case distinguished by presence of '-val' (prim types already handled by dispatch).
      # TODO: handle Dict case.
      items = [str(v) for v in self['-val']]
    except KeyError: # generic case.
      def repr_item(p):
        k, v = p
        sv = v._repr(d, obj_set) if isinstance(v, Stork) else repr(v)
        return '-{}={}'.format(k, sv)
      items = sorted(repr_item(p) for p in self.items() if p[0] not in _repr_excluded_tags)
    r = '({} {})'.format(tnv, ' '.join(items)) if items else '({})'.format(tnv)
    obj_set.remove(self)
    return r

  def __repr__(self):
    return self._repr(2, set())

  def __str__(self):
    'just like python, we must distinguish between the quoted string repr and the actual string value.'
    global Str
    if self['-type-of'] is Str:
      return self['-val']
    return repr(self)


  def __getattr__(self, attr_key):
    '''
    allow access of dictionary items with dot syntax. '_' is converted to '-'.
    '''
    key = attr_key.replace('_', '-')
    try:
      return self[key]
    except KeyError as e:
      errFL('object access key error: {}\n  obj {:x}: {}', e, id(self), self)
      raise e

  def __setattr__(self, attr_key, attr_val):
    raise AttributeError('Stork object attributes must be set with dictionary access')


# assemble the metatype graph:
# the graph of objects necessary to make Type a proper, self-describing Stork object.
Type = Stork()
Type['-type-of'] = Type


mk_Obj_check = None # can replace this with real object validity predicate function later.

def mk_Obj_with_items(stork_type, items):
  '''
  make a stork object of any type and arbitrary attributes.
  '''
  obj = Stork((k.replace('_', '-'), v) for k, v in items)
  obj['-type-of'] = stork_type
  assert not mk_Obj_check or mk_Obj_check(obj)
  return obj


def mk_Obj(stork_type, **kw):
  return mk_Obj_with_items(stork_type, kw.items())


def mk_prim(stork_type, val):
  '''
  make a stork primitive object.
  '''
  prim = Stork({'-type-of':stork_type, '-val':val})
  assert not mk_Obj_check or mk_Obj_check(prim)
  return prim


Str = mk_Obj(Type)
def mk_Str(py_str):
  return mk_prim(Str, py_str)


opt_of_type_none = Stork()

Type['name']  = mk_Str('Type')
Str['name']   = mk_Str('Str')
Type['parent']  = opt_of_type_none
Str['parent']   = opt_of_type_none


def mk_Type(py_name, parent=opt_of_type_none, kind=None):
  'parent defaults to none; kind is optional for bootstrapping process only.'
  return mk_Obj(Type, name=mk_Str(py_name), parent=parent, kind=kind)


TI_Opt_none, TI_Opt_a = range(2)
None_ = mk_Type('None')
none = mk_Obj(None_)

Opt_of_Type = mk_Type('(Opt -El=Type)')
opt_of_type_none['-type-of'] = Opt_of_Type
opt_of_type_none['-tag'] = TI_Opt_none
opt_of_type_none['none'] = none

Int = mk_Type('Int')
def mk_Int(py_int):
  assert isinstance(py_int, int)
  return mk_prim(Int, py_int)


Bool = mk_Type('Bool')
true  = mk_prim(Bool, True)
false = mk_prim(Bool, False)
def mk_Bool(val):
  return true if val else false


Opaque = mk_Type('Opaque')
def mk_Opaque(py_val):
  return mk_prim(Opaque, py_val)

opaque_str    = mk_Opaque(str)
opaque_int    = mk_Opaque(int)
opaque_bool   = mk_Opaque(bool)
opaque_object = mk_Opaque(object)
opaque_list   = mk_Opaque(list)
opaque_dict   = mk_Opaque(dict)
opaque_set    = mk_Opaque(set)
opaque_env    = mk_Opaque(StorkEnv)


repr_dispatch = {
  Str     : lambda o: ellipse_repr(o['-val'], 64),
  Int     : lambda o: repr(o['-val']),
  Bool    : lambda o: 'true' if o['-val'] else 'false',
  Opaque  : lambda o: '(Opaque {})'.format(type(o['-val'])),
}


Type_el = mk_Type('Type-el')
# [-name:Str Type] pair describing a member of a union or struct type.
def mk_Type_el(name, type):
  assert name._type_of is Str
  assert type._type_of is Type
  return mk_Obj(Type_el, name=name, type=type)


Array_of_Type_el = mk_Type('(Array -El=Type-el)')
def mk_Array_of_Type_el(py_list):
  return mk_prim(Array_of_Type_el, py_list)

def _mk_Type_els(*items):
  return mk_Array_of_Type_el([mk_Type_el(mk_Str(n), t) for n, t in items])


Type_kind = mk_Type('Type-kind')

# Type-kind is a union type with the following variants:
Type_kind_unit      = mk_Type('Type-kind-unit')     # singleton types.
Type_kind_prim      = mk_Type('Type-kind-prim')     # primitive types.
Type_kind_prim_gen  = mk_Type('Type-kind-prim-gen') # primitive generic types.
Type_kind_union     = mk_Type('Type-kind-union')    # union types (tagged unions);  alias of <List Type_el>.
Type_kind_struct    = mk_Type('Type-kind-struct')   # struct types (structures); alias of <List Type_el>.
Type_kind_vec       = mk_Type('Type-kind-vec')      # vector types.
Type_kind_var       = mk_Type('Type-kind-var')      # type variable.
Type_kind_alias     = mk_Type('Type-kind-alias')    # alias types.
Type_kind_class     = mk_Type('Type-kind-class')    # classes (interfaces).

_variants_of_Type_kind = _mk_Type_els(
  ('unit',      Type_kind_unit),
  ('prim',      Type_kind_prim),
  ('prim-gen',  Type_kind_prim_gen),
  ('union',     Type_kind_union),
  ('struct',    Type_kind_struct),
  ('vec',       Type_kind_vec),
  ('alias',     Type_kind_alias),
  ('var',       Type_kind_var),
  ('class',     Type_kind_class))

TI_unit, TI_prim, TI_prim_gen, TI_union, TI_struct, TI_vec, TI_alias, TI_var, TI_class = range(9)

# now fill out the missing kinds for all objects created thus far.

# the kind of Type-kind.
Type_kind['kind'] = mk_Obj(Type_kind, _tag=TI_union, union=_variants_of_Type_kind)


# now that Type-kind has a kind, we can refer to tags by name alone; mk_Morph will find the index.
def mk_morph(type_, variant_name, obj):
  'make an instance of a stork union type; requires a py str variant name.'
  assert isinstance(variant_name, str)
  variants = type_.kind.union._val # py list; union member is an (Array -El=Type-el).
  tag = None
  for i, type_el in enumerate(variants):
    #errR(type_el, label='EL {}'.format(i))
    if type_el.name._val == variant_name:
      tag = i
      break
  if tag is None:
    errFL('bad morph name: {}\n  type: {}', variant_name, type_)
  morph = Stork({'-type-of':type_, '-tag':tag, variant_name:obj})
  assert not mk_Obj_check or mk_Obj_check(morph)
  return morph


def mk_kind_prim(host_type):
  return mk_morph(Type_kind, 'prim', mk_Obj(Type_kind_prim, host_type=host_type))


def mk_kind_prim_gen(type, host_type):
  return mk_morph(Type_kind, 'prim-gen', mk_Obj(Type_kind_prim_gen, type=type, host_type=host_type))


def mk_kind_union(*items):
  return mk_morph(Type_kind, 'union', _mk_Type_els(*items))


def mk_kind_struct(*items):
  return mk_morph(Type_kind, 'struct', _mk_Type_els(*items))


def mk_kind_alias(type):
  return mk_morph(Type_kind, 'alias', mk_Obj(Type_kind_alias, type=type))


# create canonical instances and a tagged morph for each of the unit types.
_unit_of_Unit   = mk_Obj(Type_kind_unit)
_unit_of_Var    = mk_Obj(Type_kind_var)
_unit_of_Class  = mk_Obj(Type_kind_class) # TODO: change this once classes are better defined.

_morph_of_Unit  = mk_morph(Type_kind, 'unit',  _unit_of_Unit)
_morph_of_Var   = mk_morph(Type_kind, 'var',   _unit_of_Var)
_morph_of_Class = mk_morph(Type_kind, 'class', _unit_of_Class)

# unit kinds
# TODO: Type_kind_class will have a different kind once classes are better defined.
for _Unit in None_, Type_kind_unit, Type_kind_var, Type_kind_class:
  _Unit['kind'] = _morph_of_Unit

# prim kinds
Str['kind']     = mk_kind_prim(opaque_str)
Int['kind']     = mk_kind_prim(opaque_int)
Bool['kind']    = mk_kind_prim(opaque_bool)
Opaque['kind']  = mk_kind_prim(opaque_object)

# prim-gen kinds.
Array_of_Type_el['kind'] = mk_kind_prim_gen(type=Type_el, host_type=opaque_list)

# union kinds.
Opt_of_Type['kind'] = mk_kind_union(('none', None_), ('a', Type))

# struct kinds.
Type['kind']                = mk_kind_struct(('name', Str), ('parent', Opt_of_Type), ('kind', Type_kind))
Type_el['kind']             = mk_kind_struct(('name', Str), ('type', Type))
Type_kind_prim['kind']      = mk_kind_struct(('host-type', Opaque))
Type_kind_prim_gen['kind']  = mk_kind_struct(('type', Type), ('host-type', Opaque))
Type_kind_alias['kind']     = mk_kind_struct(('type', Type)) # TODO: what else? resolved field?
Type_kind_vec['kind']       = mk_kind_struct(('dim', Int), ('type', Type))

# alias kinds.
Type_kind_union['kind']  = mk_kind_alias(type=Array_of_Type_el)
Type_kind_struct['kind'] = mk_kind_alias(type=Array_of_Type_el)

# no var kinds yet.

# kinds are now complete; still need to fill in type parent fields, but define helpers first.


def name_from_Type(t, counter=None):
  'generate a name from a type, optionally numbering names with a counter.'
  n = t.name._val.lower()
  if counter is None:
    return mk_Str(n)
  else:
    i = counter[n]
    counter[n] += 1
    return mk_Str('{}{}'.format(n, i) if i else n)


def auto_name_Type(p_or_t, counter):
  '''
  generate a field name Str from a (name, type) pair or a type, with the help of a Counter.
  if it is the first occurrence of the type, then the name is simply that of the type;
  otherwise it is numbered from 1.
  thus the first occurrence is implicitly zero-indexed.
  TODO: for generic types, the type name used should be the generic name.
  '''
  if isinstance(p_or_t, tuple): # pair.
    n, t = p_or_t
    nv = n if isinstance(n, str) else n._val
    if nv: # for non-empty custom, names, just count and return.
      assert nv not in counter # note that this does not handle numbered names well.
      counter[nv] += 1
      return mk_Str(nv), t # could sometimes reuse p_or_t.
  else: # lone type
    t = p_or_t
  assert isinstance(t, Stork)
  return name_from_Type(t, counter), t


def auto_name_Types(pairs_or_types):
  'makes (Array -El=Type-el). used by mk_Type_union and mk_Type_struct.'
  c = Counter()
  return mk_Array_of_Type_el([mk_Type_el(*auto_name_Type(t, c)) for t in pairs_or_types])


def mk_Type_unit_and_val(py_name):
  t = mk_Type(py_name, kind=_morph_of_Unit)
  return t, mk_Obj(t)

def mk_Type_prim(py_name, host_type):
  return  mk_Type(py_name, kind=mk_kind_prim(host_type=host_type))

def mk_Type_prim_gen(py_name, type, host_type):
  assert type._type_of is Type
  return mk_Type(py_name, kind=mk_kind_prim_gen(type=type, host_type=host_type))

def mk_Type_union(py_name, *variants):
  v = auto_name_Types(variants)
  return mk_Type(py_name, kind=mk_morph(Type_kind, 'union', v))

def mk_Type_struct(py_name, *fields):
  f = auto_name_Types(fields)
  return mk_Type(py_name, kind=mk_morph(Type_kind, 'struct', f))

def mk_Type_alias(py_name, type):
  return mk_Type(py_name, kind=mk_kind_alias(type=type))

def mk_Type_class(py_name):
  return mk_Type(py_name, kind=_morph_of_Class)

@memoize
def mk_Type_var(py_name):
  return mk_Type(py_name, kind=_morph_of_Var)


El = mk_Type_var('El')
Opt = mk_Type_union('Opt', None_, ('a', El)) # [-none -a] union template.
Opt_of_Type['parent'] = mk_morph(Opt_of_Type, 'a', Opt)

# TODO: unused; remove?
#Opt_of_Obj = derive_Type(Opt, El, Obj)
#opt_of_obj_none = mk_morph(Opt_of_Obj, 'none', none)

Array = mk_Type_prim_gen('Array', El, host_type=opaque_list)
Dict  = mk_Type_prim_gen('Dict',  El, host_type=opaque_dict)
Set   = mk_Type_prim_gen('Set',   El, host_type=opaque_set)


# metatype graph is now complete.


# utility functions.


def name_of_morph(m):
  return m._type_of.kind.union._val[m._tag].name._val


def member_of_morph(m):
  return m[name_of_morph(m)]


def resolve_Type_alias(t):
  'resolve a type alias to a non-alias type.'
  k = t.kind
  if k._tag != TI_alias:
    return t
  r = k.alias.type
  _eh.checkS(r.kind._tag != TI_alias, _eh.Error,
    'resolve_Type_alias encountered nested Type-alias:', t)
  return r


def resolve_Type_parent(t):
  'resolve a parent type to itself or its parent.'
  p = t.parent
  if p._tag == TI_Opt_none:
    return mk_morph(Opt_of_Type, 'a', t)
  else:
    r = p.a
    _eh.checkS(r.parent._tag == TI_Opt_none, _eh.Error,
      'resolve_Type_parent encountered nested parent:', t)
    return p


def is_Type_conforming(t_act, t_exp):
  if t_exp.kind._tag == TI_class:
    return True # class conformance checks not yet implemented.
  else:
    return t_act is t_exp


def is_Obj_conforming(obj, t):
  return is_Type_conforming(obj._type_of, t)


def vars_of_members(members):
  a = []
  for e in members._val:
    a.extend(vars_of_Type(e.type))
  return tuple(a)


@memoize_rec(()) # prevent infinite recursion by returning empty tuple for inner calls.
def vars_of_Type(type):
  k = type.kind
  kt = k._tag
  if kt == TI_unit:
    return ()
  if kt == TI_prim:
    return ()
  if kt == TI_prim_gen:
    return vars_of_Type(k.prim_gen.type)
  if kt == TI_union:
    return vars_of_members(k.union)
  if kt == TI_struct:
    return vars_of_members(k.struct)
  if kt == TI_vec:
    return vars_of_Type(k.vec.el_type)
  if kt == TI_alias:
    return vars_of_Type(k.alias.type)
  if kt == TI_var:
    return (type,)
  if kt == TI_class:
    return () # TODO: this may become more complicated.
  raise AssertionError('unknown kind')


def var_in_Type(type, var):
  return var in vars_of_Type(type)


def _derive_Type_el(type_el, var, val, visited):
  n = type_el.name
  t = type_el.type
  d_t = _derive_Type(t, var, val, visited)
  # reuse type_el if possible
  return type_el if d_t is t else mk_Type_el(n, d_t)


def _derive_Array_of_Type_el(array, var, val, visited):
  '''
  create a new List of Type-el by substituting a type val for a type var.
  Returns as second argument whether or not result is identical to array argument.
  '''
  d_a = [_derive_Type_el(e, var, val, visited) for e in array._val]
  return (array, True) if d_a == array else (mk_Array_of_Type_el(d_a), False)


def _derive_Type(t, var, val, visited):
  def same():
    visited[t] = t
    return t
  n = t.name._val
  #errSL('derive:', n, var.name._val, val.name._val, t.kind._tag)
  if t is var:
    return val
  try:
    vt = visited[t]
  except KeyError:
    pass # main case handled below after else clause returns.
  else: # prevent infinite recursion over recursive types.
    if vt is None: # recursive type detected.
      # must decide now whether this type will be replaced,
      # so that the root and leaf recursive calls return the same value.
      if not var_in_Type(t, var):
        return same()
      dt = mk_Type('DERIVED-REC', kind=_morph_of_Unit) # temporary kind passes check.
      visited[t] = dt # memoize the return value so that root call may use it too.
      #errSL('recursive:', n, dt.name._val)
      return dt
    else: # vt is complete or in processing by caller at some level.
      #errSL('visited:', n)
      return vt
  # main case
  #errSL('deriving:', n)
  visited[t] = None # marker to detect recursive types.
  k = t.kind
  kt = k._tag

  if kt == TI_prim_gen:
    pg = k.prim_gen
    el_t = pg.type
    d_el_t = _derive_Type(el_t, var, val, visited)
    if d_el_t is el_t:
      return same()
    #print('D', d_el_t)
    dk = mk_kind_prim_gen(type=d_el_t, host_type=pg.host_type)
  
  elif kt in (TI_union, TI_struct):
    mn = name_of_morph(k)
    a = k[mn]
    d_a, is_identical = _derive_Array_of_Type_el(a, var, val, visited) # recurse
    if is_identical:
      return same()
    dk = mk_morph(Type_kind, mn, d_a)
  
  elif kt == TI_vec:
    el_t = k.vec.type
    d_el_t = _derive_Type(el_t, var, val, visited)
    if d_el_t is el_t:
      return same()
    dk = mk_morph(Type_kind, 'vec', mk_Obj(Type_kind_vec, dim=k.dim, type=d_el_t))
  
  else: # remaining kinds cannot have derivatives.
    return same()

  # get the recursive place-holder or create a new derived type
  dt = visited[t] or mk_Type('DERIVED', kind=_morph_of_Unit) # temporary kind passes check.
  assert not dt is t
  # overwrite name and fake kind of DERIVED.
  var_name = var.name._val
  val_name = val.name._val
  d_n = '({} -{}={})'.format(n, var_name, val_name)
  #errSL('derived:', d_n)
  dt['name'] = mk_Str(d_n)
  dt['parent'] = resolve_Type_parent(t)
  dt['kind'] = dk 
  visited[t] = dt
  assert not mk_Obj_check or mk_Obj_check(dt)
  return dt


@memoize
def derive_Type(t, var, val):
  # TODO: this does not fully memoize properly if same type is derived with different intermediate order.
  dt = _derive_Type(t, var, val, visited={})
  if dt is t:
    error('type var {} not in parent type: {}', var, t)
  return dt



# obj is the empty interface; all objects conform to Obj.
Obj = mk_Type_class('Obj')

Array_of_Obj = derive_Type(Array, El, Obj)

def mk_Array_of_Obj(py_list):
  assert isinstance(py_list, list)
  return mk_prim(Array_of_Obj, py_list)


type_types = [
  Type,
  Type_kind,
  Int,
  Str,
  Bool,
  Opaque,
  None_,
  El,
  Type_el,
  Array,
  Dict,
  Set,
  Obj,
  Opt,
]

# derived types are separate because they are not bound into the global environment.
derived_types = [
  Opt_of_Type,
  Array_of_Type_el,
  #Opt_of_Obj,
  Array_of_Obj,
]

type_instances = [
  ('none', none),
  ('true', true),
  ('false', false),
]

# miscellaneous
str_empty = mk_Str('')

