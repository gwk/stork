#!/usr/bin/env sh
# Copyright 2010 George King.
# Permission to use this file is granted in stork/license.txt.

set -e
ROOT=$(dirname $0)

flags=''
while [[ "$1" = -* ]]; do
  flags="$flags $1"
  shift
done

tests="$@"
[[ -z "$tests" ]] && tests="$ROOT/test"

# first make sure that boot runs at all.
"$ROOT/boot/boot.py" -safe -c ''

set -x
~/work/gloss/bin/sys-test.py -fast -interpreters '.stork' "$ROOT/boot/boot.py -safe" $flags - \
$tests
