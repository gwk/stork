Copyright 2013 George King.
Permission to use this file is granted in stork/license.txt.

Stork is a programming language that aspires to help programmers create more reusable code. It is inspired by Lisp as well as a variety of strongly typed languages. 

Stork is licensed under the ISC License (similar to two clause BSD). See stork/license.txt for more information, including compatibility notes.

Stork is currently in an early experimental stage.

For a detailed description of Stork, see doc/about.txt.


-----
Setup

Stork depends on the Gloss python library, published at https://github.com/gwk/gloss. You can install the library using ~/path/to/gloss/install/gloss-install-py.sh, or just modify your python path in the shell, e.g.:
  export PYTHONPATH=~/path/to/gloss/python

Please do not hesitate to contact me if you have issues with setup.
